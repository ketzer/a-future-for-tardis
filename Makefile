PROJECT=maketest
docker: build
build: Dockerfile
	docker build -t $(PROJECT) .
rebuild:
	docker build --no-cache -t $(PROJECT) .
save-docker: $(PROJECT).tar.gz
$(PROJECT).tar.gz:
	docker save $(PROJECT):latest | gzip > $@docker: build
build: Dockerfile
	docker build -t $(PROJECT) .
rebuild:
	docker build --no-cache -t $(PROJECT) .
save-docker: $(PROJECT).tar.gz
$(PROJECT).tar.gz:
	docker save $(PROJECT):latest | gzip > $@

singularity: $(PROJECT).sif
$(PROJECT).sif: docker
	singularity build $@ docker-daemon://$(PROJECT):latest
